#Conectarse a un SFTP
ssh user@host
#para enviar un archivo de un pc local a un remoto
scp nombreArchivo user@host:/home/path
#para enviar desde un remoto a un local
scp  user@host:/home/path C:\Users\nombreUsuario
#Guardar el resultado de una comando en un archivo
ls > archivo.txt 
#Insertar sin borrar en el archivo
ls >> archivo.txt

